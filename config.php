<?php

/* SOCKET PORT */
define("SERVER_PORT", 8080);

/* GAME SIZE */
define("CELL_SIZE", 30);
define("GRID_SIZE", 21);
define("SAFE_AREA_SIZE", 3);
define("BOMB_RANGE", 2);

/* PREFIXES */
define("PREFIX_INFO", 	'[' . LIGHT_CYAN . 'INFO ' . WHITE . ']> ');
define("PREFIX_ERROR", 	'[' . RED . 'ERROR' . WHITE . ']> ');
define("PREFIX_OK", 	'[' . LIGHT_GREEN . 'OK   ' . WHITE . ']> ');

/* EVENTS */
define("EVENT_SET_USERNAME", 0); //le client envois son username
define("EVENT_CHALLENGE_DEMAND", 1); //le client demander à défier X
define("EVENT_GRID_SIZE", 2); //le serveur envois les dimensions du grid au client
define("EVENT_BREAKABLE_POS", 3); //le serveur envois les positions des blocs incassambles
define("EVENT_USERS_LIST_UPDATE", 4); //le serveur broadcast la liste des utilisateurs en ligne
