<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class core
{
    private $port;
    private $websocket;

    public function __construct(int $port)
    {
        $this->port = $port;
        $this->websocket = IoServer::factory(new HttpServer(new WsServer(new Socket())), $port);
        UTILS::pinfo("New server created (port: " . $this->port . ")");
    }

    public function start() : void
    {
        UTILS::psuccess("Running server...");
        $this->websocket->run();
        UTILS::pinfo("Server exited!");
    }
}
