<?php

class UTILS
{
    public static function ptext(string $text, bool $new_line = true)
    {
        echo($text . ($new_line ? PHP_EOL : ''));
    }

    public static function pinfo(string $text, bool $new_line = true)
    {
        UTILS::ptext(PREFIX_INFO . $text, $new_line);
    }

    public static function perror(string $text, bool $new_line = true)
    {
        UTILS::ptext(PREFIX_ERROR . $text, $new_line);
    }

    public static function psuccess(string $text, bool $new_line = true)
    {
        UTILS::ptext(PREFIX_OK . $text, $new_line);
    }

    /**
     * Generates a random string
     * @param   int     $len  String length
     * @return  string        Random generated string
     */
    public static function str_rnd(int $len) : string
    {
        $char_list = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return substr(str_shuffle(str_repeat($char_list, ceil($len / strlen($char_list)) )), 1, $len);
    }
}
