<?php

class session extends game
{
  private int $id;
  private int $first_player_id;
  private int $second_player_id;

  public function __construct(int $first_player_id, int $second_player_id)
  {
    $this->id = rand();
    $this->first_player_id = $first_player_id;
    $this->second_player_id = $second_player_id;

    UTILS::pinfo(sprintf("Game session created (id: %s, p1: %s, p2: %s)", $this->id, $this->first_player_id, $this->second_player_id));
  }


};
