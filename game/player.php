<?php

class player//extends session
{
  private bool      $connected;
  private string    $username;
  private position  $pos;

  public  function  __construct()
  {
    //$this->username = $username;
    $this->connected = false;
    $this->pos      = new position();
    //UTILS::pinfo("New user created (username: $username)");
  }

  public function set_username(string $username) : void
  {
    $this->username = $username;
  }

  public function get_username() : string
  {
    return $this->username;
  }

  public function set_connected(bool $state) : void
  {
    $this->connected = $state;
  }

  public function is_connected() : bool
  {
    return $this->connected;
  }

};
