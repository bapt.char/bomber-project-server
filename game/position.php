<?php

class position
{
  public int $x;
  public int $y;

  public function __construct()
  {
    $this->x = -1;
    $this->y = -1;
  }

  public function move_left()
  {
    //TODO: Make sure player is not out of board
    $this->x--;
  }

  public function move_right()
  {
    //TODO: Make sure player is not out of board
    $this->x++;
  }

  public function move_up()
  {
    //TODO: Make sure player is not out of board
    $this->y--;
  }

  public function move_down()
  {
    //TODO: Make sure player is not out of board
    $this->y++;
  }
}
