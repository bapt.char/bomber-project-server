<?php

class game
{
  private   array   $session_list = array();
  private   array   $guest_list   = array();

  public function __construct()
  {
    UTILS::pinfo("Game class initialized");
  }

  public function send_grid_data($conn) : void
  {
    $packet = array(
      'evt' => EVENT_GRID_SIZE,
      'cell_size' => CELL_SIZE,
      'grid_size' => GRID_SIZE
    );

    $conn->send(json_encode($packet));
  }

  public function sent_breakable_block_size($conn) : void
  {
    $packet = array(
      'evt' => EVENT_BREAKABLE_POS,
      'positions' => $this->gen_new_random_grid()
    );

    $conn->send(json_encode($packet));
  }

  public function broadcast_user_list(&$conn, &$list) : void
  {
    //broadcast new online users list
    $users_list = array();

    foreach($list as $key => $curcon)
    {
      if ($curcon->player->is_connected())
      {
        $users_list[] = array(
          'id' => $curcon->resourceId,
          'username' => $curcon->player->get_username()
        );
      }
    }

    $packet = array(
      'evt' => EVENT_USERS_LIST_UPDATE,
      'users' => $users_list
    );

    $this->broadcast($list, $conn, json_encode($packet), true);
  }

  public function broadcast($list, $sender, string $data, bool $include_self = false) : void
  {
      foreach ($list as $id => $client)
      {
          if (!$include_self && $sender == $client)
              continue;

          $client->send($data);
      }
  }

  public function gen_new_random_grid() : array
  {
    $positions = array();

    for($x = 1; $x < GRID_SIZE - 1; $x++)
    {
      for($y = 1; $y < GRID_SIZE - 1; $y++)
      {
        if ($x < SAFE_AREA_SIZE && $y < SAFE_AREA_SIZE)
          continue;

        if ($x > GRID_SIZE-SAFE_AREA_SIZE-1 && $y > GRID_SIZE-SAFE_AREA_SIZE-1)
          continue;

        if ((($x % 2) || ($y % 2)) && rand(0, 3) >= 2)
        {
          $positions[] = array(
            'x' => $x,
            'y' => $y
          );
        }
      }
    }

    return $positions;
  }

  public function create_sessions(int $id_1, int $id_2) : void
  {

    $session_list[] = new session($id_1, $id_2);
    


  }





};
