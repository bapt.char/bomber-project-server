<?php

/* libraries */
include('vendor/autoload.php');

/* config files */
include('colors.php');
include('config.php');

/* utils */
include('utils.php');

/* main files */
include('socket.php');
include('core.php');

/* game files */
include('game/game.php');
include('game/session.php');
include('game/player.php');
include('game/position.php');

/* init */
$core = new core(SERVER_PORT);
$core->start();
