<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Socket implements MessageComponentInterface
{
    protected   array    $clients = array();
    private     game     $game;

    public function __construct()
    {
        $this->game = new game();
    }

    /**
     * Called for each message received
     * @param   ConnectionInterface   $conn   User connection
     * @param   string                $msg    Message
     */
    public function onMessage(ConnectionInterface $conn, $msg)
    {
        $ws_id = $conn->resourceId;
        UTILS::pinfo("($ws_id) New message: $msg");

        $json = json_decode($msg);

        if (json_last_error() != JSON_ERROR_NONE)
        {
            UTILS::perror("($ws_id) Json couldn't be parsed (" . json_last_error() . ")");
            return;
        }

        UTILS::pinfo("($ws_id) Event was: " . $json->evt);

        switch($json->evt)
        {
          case EVENT_SET_USERNAME:
          {
            $username = $json->username;

            if (strlen($username) > 30)
              return;

            //set user online
            $this->clients[$ws_id]->player->set_username($username);
            $this->clients[$ws_id]->player->set_connected(true);

            //update online user list for all clients
            $this->game->broadcast_user_list($this->clients[$ws_id], $this->clients);

            break;
          }
          case EVENT_CHALLENGE_DEMAND:
          {
            //$ws_id veux défier $json->id

            

            break;
          }
          default:
            break;
        }
    }

    /**
     * Called when a connection is opened
     * @param   ConnectionInterface   $conn   User connection
     * @return  void
     */
    public function onOpen(ConnectionInterface $conn) : void
    {
        $ws_id = $conn->resourceId;

        $this->clients[$ws_id] = $conn;

        $this->clients[$ws_id]->player = new player();

        UTILS::pinfo("($ws_id) Connected");

        /* SEND GAME SIZE */
        $this->game->send_grid_data($conn);

        /* SEND BREAKABLE BLOCK POSITIONS */
        $this->game->sent_breakable_block_size($conn);

        //update online user list for all clients
        $this->game->broadcast_user_list($this->clients[$ws_id], $this->clients);

    }

    /**
     * Called when a connection is closed
     * @param   ConnectionInterface   $conn   User connection
     * @return  void
     */
    public function onClose(ConnectionInterface $conn) : void
    {
        $ws_id = $conn->resourceId;
        $this->clients[$ws_id]->player->set_connected(false);

        //update online user list for all clients
        $this->game->broadcast_user_list($this->clients[$ws_id], $this->clients);

        unset($this->clients[$ws_id]);
        UTILS::pinfo("($ws_id) Disconnected");


    }

    /**
     * Called when an error occurs
     * @param   ConnectionInterface   $conn   User connection
     * @param   Exception             $e      Error exception
     * @return  void
     */
    public function onError(ConnectionInterface $conn, Exception $e) : void
    {
        $ws_id = $conn->resourceId;
        UTILS::perror("($ws_id) Error (" . $e->getMessage() . ")");
        $conn->close();
    }

    private function answer_error(ConnectionInterface $conn, int $error) : void
    {
        $packet = array(
            'evt' => REQUEST_ERROR,
            'err' => $error
        );

        //$conn->send(CYPHER::encrypt($packet));
    }


}
